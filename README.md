# GitLab CI template for Dependency Track

This project implements a GitLab CI/CD template to collect and send SBOM reports to
a [Dependency Track](https://dependencytrack.org/) server.

## Usage

This template can be used both as
a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration)
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/dependency-track/gitlab-ci-dependency-track@1.0.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      base-api-url: "https://dependency-track.my-company.org/api"
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: "to-be-continuous/dependency-track"
    ref: "1.0.0"
    file: "/templates/gitlab-ci-dependency-track.yml"

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  DEPTRACK_BASE_API_URL: "https://dependency-track.my-company.org/api"
```

## Understanding the Dependency Track template

The template will upload to Dependency Track every SBOM file with the extension `.cyclonedx.json` in the project
directory.
Also this template supports the following ways of managing DT projects:

1. Automatic creation of the project by giving the project name and version, and parent information if relevant.
2. Full creation of project tree
3. No automatic creation (no extra permissions needed)

### `auto-create` mode (default)

By default, this template will activate the Dependency Track option to automatically create the project on DT if it
doesn't exists.
Your API key will require the following permissions:

* [BOM_UPLOAD](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L29)
* [PROJECT_CREATION_UPLOAD](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L39)

### `full` mode

This mode will allows you to manage the project tree, by creating the parent projects if they don't exists.
The `parent-name` and `parent-version` options can be a path (`/` as delimiter) to create several levels of hierarchy.
For example, this configuration:

```yaml
    inputs:
      parent-name: "product-A/microservice-1"
      parent-version: "main/1.2.3"
```

will create the DT project `product-A` with version `main` and project `microservice-1` with version `1.2.3` as a child
project of `product-A`.
Project `microservice-1` will be the parent of the imported SBOMs.

If you change the project version, the template will clone the existing DT project with the new version like your will
manually doing it on the web interface.

This option, will requires the following permissions:

* [BOM_UPLOAD](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L29)
* [VIEW_PORTFOLIO](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L30)
* [PORTFOLIO_MANAGEMENT](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L31)

### `none` mode

This option requires the least permissions, the template will upload the SBOMs to DT and fail if the project doesn't
exists. Requires only the [BOM_UPLOAD](https://github.com/DependencyTrack/dependency-track/blob/master/src/main/java/org/dependencytrack/auth/Permissions.java#L29) permission.

## Configuration

The Dependency Track template uses the following configuration.

| Input / Variable                                     | Description                                                                                                                               | Default value                                       |
|------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------|
| `base-image` / `DEPTRACK_BASE_IMAGE`                 | The Docker image used to upload SBOM reports (requires: `curl`,`jq` & `bash`)                                                             | `registry.hub.docker.com/badouralix/curl-jq:debian` |
| `base-api-url` / `DEPTRACK_BASE_API_URL`             | Dependency Track server base API url (includes `/api`)                                                                                    | _none_ (required)                                   |
| :lock: `DEPTRACK_API_KEY`                            | Dependency Track API key                                                                                                                  | _none_ (required)                                   |
| `project-uuid` / `DEPTRACK_PROJECT_UUID`             | Dependency Track project UUID (optional)                                                                                                  | _none_                                              |
| `creation-mode` / `DEPTRACK_CREATION_MODE`           | Determines whether the project should be automatically created when it doesn't exist, possible values are `none`, `auto-create` or `full` | `auto-create`                                       |
| `project-name` / `DEPTRACK_PROJECT_NAME`             | Dependency Track project name (ignored if `project-uuid` is set)                                                                          | `$CI_PROJECT_NAME`                                  |
| `project-version` / `DEPTRACK_PROJECT_VERSION`       | Dependency Track project version (ignored if `project-uuid` is set)                                                                       | _none_                                              |
| `project-classifier` / `DEPTRACK_PROJECT_CLASSIFIER` | Dependency Track project classifier (optional)                                                                                            | `APPLICATION`                                       |
| `parent-name` / `DEPTRACK_PARENT_NAME`               | Dependency Track parent name (optional)                                                                                                   | _none_                                              |
| `parent-version` / `DEPTRACK_PARENT_VERSION`         | Dependency Track parent version (optional)                                                                                                | _none_                                              |

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them
   as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#create-a-custom-variable-in-the-ui):
    - [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-custom-variable) to prevent them from being
      inadvertently
      displayed in your job logs,
    - [**protected**](https://docs.gitlab.com/ee/ci/variables/#protect-a-custom-variable) if you want to secure some
      secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret
   contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#masked-variable-requirements),
   simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
   it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).
